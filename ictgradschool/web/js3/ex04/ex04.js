"use strict";

// TODO Create object prototypes here


function MusicAlbum (title, artist, year, genre, tracks) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = tracks;
    this.getAlbums = function() {
return this.title +" " + this.artist + " " + this.year
    };
    this.printAlbums = function () {
        return " Album: " + this.title + ", released in " + this.year + " by " + this.artist + " " + this.tracksToString()
      };
  
      this.tracksToString = function () { 
          var list = ""; 
          for ( var i = 0; i< tracks.length; i++) {
        
             list += "\n - " + this.tracks[i] }
       return list;
      };
  
};
var album1 = new MusicAlbum (
    "1989", "Taylor Swift", "2014", "Pop",
    ["Welcome to New York", "Blank Space","Style","Out of the Woods","All You Had to Do Was Stay","Shake It Off",
    "I Wish You Would","Bad Blood","Wildest Dreams"," How You Get the Girl","This Love","I Know Places","Clean"]
);
var album2= new MusicAlbum (
    "PTX.Vol.IV-Classics", "Pentatonix", "2017", "Classics",
    ["Bohemian Rhapsody", "Imagine","Take On Me"]
);
// TODO Create functions here


// TODO Complete the program here
// console.log (album1.getAlbums());
// console.log (album1.printAlbums());
console.log (album1.tracksToString());
