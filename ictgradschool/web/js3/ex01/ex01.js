"use strict";

var text = "This is some text.";
text = text.toUpperCase();

var frequencies = [];

for (var i = 0; i < text.length; i++) {

    var character = text.charAt(i);

    // TODO If the current character already has an entry in the array (i.e. frequencies[character] != undefined),
    // increment that entry. Otherwise, add a new entry with the initial value of 1.
    if (frequencies[character] != undefined) {
        frequencies[character] = frequencies[character] + 1;
    } else {
        frequencies[character] = 1;

    };
};

for (var charact in frequencies) {
    console.log("'" + charact + "'" + " apprears " + frequencies[charact] + " times in the text");
}


/* frequencies = [
    'T': 1,
    'H' : 1
....
] */


// TODO Print out the result
console.log("Text: " + text);
