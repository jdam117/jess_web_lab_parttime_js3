"use strict";

// Change this to play around.
var value = 30;

// Is the value greater than 10 AND less than 100?
var gt10Andlt100 = (value > 10 && value < 100);
console.log(gt10Andlt100);

// Is the value greater than 10 OR the value is equal to 1?
var gt10Oreq1 = (value > 10 || value == 1);
console.log(gt10Oreq1);

// Is the value NOT greater than 10?
var notgt10 = !(value > 10);
console.log(notgt10);

// Is the value less than or equal to 10? (This is equivalent to #3)
var leq10 = (value <= 10);
console.log(leq10);

// Is the value NOT greater than 10 AND NOT equal to 1?
var notgt10AndNoteq1 = (!(value > 10) && value != 1);
console.log(notgt10AndNoteq1);

// Is the value NOT either greater than 10 OR equal to 1? (This is equivalent to #5)
var notgt10Oreq1 = !(value > 10 || value == 1);
console.log(notgt10Oreq1);

var x = 10;

// If x is 5, print "A".
if (x == 5) {
    console.log("A.");
}

// Otherwise, if x is between 3 and 10 (exclusive), then print "B".
else if (x > 3 && x < 10) {
    console.log("B.");
}

// Otherwise, print "C".
else {
    console.log("C.");
}
