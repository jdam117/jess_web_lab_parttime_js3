"use strict";

// Defining a "person" object, using JS Object notation.
var person = {
    name: "Dr. Nick",
    age: 42,
    address: "123 Some Street, Springfield"
};

// We can get / set the individual properties of objects like this...
console.log(person.name);
person.age = 50;

// ... or like this.
console.log(person["address"]);
person["age"] = 100;



// Defining a slightly more complex person object. This time,
// the person's address is also an object, rather than just a string.
// And this time, we've also defined a patientNames property, which is
// an array.
var complexPerson = {
    name: "Nick Riviera",
    age: 42,
    address: {
        number: 123,
        streetName: "Some Street",
        city: "Springfield"
    },
    patientNames: [
        "Anne",
        "Bob",
        "Caitlin",
        "Dave"
    ]
}

// Showing how we can get those more complex values
console.log(complexPerson.name);
console.log(complexPerson.address.streetName);
console.log(complexPerson.patientNames[1]);




// This function sets the age of the given person to the given value.
// This change WILL be visible outside of the function too (slightly
// different from if we reassigned the entire person variable).
function setAge(person, newAge) {
    person.age = newAge;
}

person.age = 10;
console.log("Age before: " + person.age); // 10
setAge(person, 20);
console.log("Age after: " + person.age); // 20




// We can add new properties to objects very easily.
person.aNewProperty = "I'm a Noob!";
console.log(person.aNewProperty);



// We can loop through all properties in an object very easily, using
// the "for.. in" loop.
console.log("Person properties:")
for (var property in person) {
    var value = person[property];
    console.log("- " + property + ": " + value);
}




// Defining another person, this time with two methods.
var personWithMethod = {
    title: "Dr.",
    firstName: "Nick",
    lastName: "Riviera",
    age: 42,
    address: "123 Some Street, Springfield",
    patientNames: [], // An initially empty array

    // This method returns the person's full name, made up of their
    // title, first name, and last name.
    getFullName: function() {

        // "this" refers to "this particular object".
        // It allows objects to access their own properties.
        var fullName = this.title + " " + this.firstName + " " + this.lastName;
        return fullName;
    },

    // This method adds a patient to the array.
    addPatient: function(patient) {
        this.patientNames[this.patientNames.length] = patient;
    } 
};

// Calling the methods look like this.
console.log(personWithMethod.getFullName());
personWithMethod.addPatient("Bob");

// Showing that addPatient worked
console.log(personWithMethod.patientNames[0]);


// Associative Arrays
// Essentially, these are "Maps", for you CS718 folk.
// We can use JavaScript objects in this way to map keys (which
// essentially will be the object's property names) to values
// which will be the object's property values).

// Storing values
var phoneBook = {};
phoneBook["Anne"] = 1234567;
phoneBook["Bob"] = 2345678;
phoneBook["Caitlin"] = 3456789;
phoneBook["Dave"] = 4567890;

// Getting values
var caitlinNumber = phoneBook["Caitlin"];
console.log("Caitlins number is: " + caitlinNumber);
console.log("Dave's number is: " + phoneBook["Dave"]);

// Looping through all values
// the "for...in" syntax will loop through each property name.
// So in this case - the people's names.
console.log("Phone Directory:");
for (var name in phoneBook) {

    console.log("- " + name + ": " + phoneBook[name]);

}