"use strict";

/**
 * This function is called "sayHi", and it prints a friendly greeting
 * to the person with the provided name.
 * 
 * This weird syntax and the @param stuff below is just comments.
 * 
 * @param {*} name the name of the person to greet.
 */
function sayHi(name) {
    console.log("Hi, " + name + "!");
}

/**
 * This function is called "add", and it will add two numbers together
 * and return the result.
 * 
 * @param {number} a the first number
 * @param {number} b the second number
 */
function add(a, b) {
    var sum = a + b;
    return sum;
}

/**
 * This function is called "mult", and it will multiply two numbers together
 * and return the result.
 * 
 * @param {number} a 
 * @param {number} b 
 */
function mult(a, b) {
    return a * b;
}

// We can pass literal values as arguments
sayHi("Dr. Nick");

// Or variables with the same name
var name = "Thomas";
sayHi(name);

// Or variables with different names. All are fine.
var anotherName = "PGCert students";
sayHi(anotherName);

// To get the return value of a function that returns a value,
// we just assign it to a variable like this.
var result = add(4, 5);
var num = 10;
var anotherResult = add(num, 8);
console.log("result = " + result + ", anotherResult = " + anotherResult);
// Or pass it directly into another function if we want.
console.log(add(78, 90));

/**
 * This function takes an operation function and two operands as arguments,
 * and will call the operation function, passing it the two operands,
 * and will print out the result.
 */
function logResult(operation, operand1, operand2) {

    // This line will call the function that was passed into the
    // operation argument.
    var result = operation(operand1, operand2);
    
    console.log(result);
}

// This shows how we can pass functions around just like variables.
// Just name the function without the brackets (). Doing this will NOT
// call the function, but treat it like a variable instead. The add
// and mult functions will actually be called on the line that's marked
// in the logResult function above.
logResult(add, 4, 5);
logResult(mult, 4, 5);