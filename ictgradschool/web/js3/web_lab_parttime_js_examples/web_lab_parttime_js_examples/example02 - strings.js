"use strict";

var myString = "Hello World";

// Strings have a length (number of chars), which can be obtained like so:
var len = myString.length;

// Each character has an index, starting at 0. You can obtain
// individual characters using charAt, like this;
console.log(myString.charAt(0)); // "H"
console.log(myString.charAt(1)); // "e"
console.log(myString.charAt(2)); // "l"
console.log(myString.charAt(3)); // "l"
console.log(myString.charAt(4)); // "o"
// etc...

// You can get the index of the first occurence of a characer using indexOf.
var indexOfL = myString.indexOf("l"); // 2
console.log(indexOfL);

// You can get part of a string - a substring - starting at the given
// index and ending JUST BEFORE the other given index.
var world = myString.substring(6, 10);
var hell = myString.substring(0, 4);
var o_W = myString.substring(4, 7);

// Only putting one argument will go from that index to the end.
var alsoWorld = myString.substring(6);

// Can convert to lower or upper case. NOTE: This does NOT
// modify the existing string, but rather, it returns a new version
// of the string with the modifications applied.
console.log(myString.toLowerCase());
console.log(myString.toUpperCase());
console.log(myString); // This will still be "Hello World".